import math


_PI = math.pi
HALF_PI = _PI / 2
PI = _PI
QUARTER_PI = _PI / 4
TAU = _PI * 2
TWO_PI = _PI * 2
DEGREES = 'degrees'
RADIANS = 'radians'
DEG_TO_RAD = _PI / 180.0
RAD_TO_DEG = 180.0 / _PI
DEGREES = 'degrees'
RADIANS = 'radians'