import random,math

_lcg_random_state = 0
_gaussian_previous = None
m = 4294967296
a = 1664525
c = 1013904223
y2 = 0

def _lcg():
    _lcg_random_state = (a * _lcg_random_state + c) % m
    return _lcg_random_state / m


def _lcgSetSeed(val=None):
    _lcg_random_state = (random.random() * m if val == None else val) >> 0

def randomSeed(seed):
    _lcgSetSeed(seed)
    _gaussian_previous = False


def random(min, max):
    rand=None

    if _lcg_random_state:
        rand = _lcg()
    else:
        rand = random.random()
  
    if type(min) == 'undefined':
        return rand
    elif type(max) == 'undefined':
        if isinstance(max,list):
            return min[math.floor(rand * len(min))]
        else:
            return rand * min
    else:
        if min > max:
            tmp = min
            min = max
            max = tmp
        
        return rand * (max - min) + min
  



def randomGaussian(mean, sd):
    global _gaussian_previous,y2
    y1 = x1 = x2 = w = None
    if (_gaussian_previous):
        y1 = y2
        _gaussian_previous = False
    else:
        while w>=1:
            x1 = random(2) - 1
            x2 = random(2) - 1
            w = x1 * x1 + x2 * x2
            
        w = math.sqrt(-2 * math.log(w) / w)
        y1 = x1 * w
        y2 = x2 * w
        _gaussian_previous = True

    m = mean | 0
    s = sd | 1
    return y1 * s + m
