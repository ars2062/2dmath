from ..constants import *
import math
_angleMode = RADIANS

def acos(ratio):
    return _fromRadians(math.acos(ratio))
def asin(ratio):
    return _fromRadians(math.asin(ratio))
def atan(ratio):
    return _fromRadians(math.atan(ratio))
def atan2(x,y):
    return _fromRadians(math.atan2(x,y))
def cos(angle):
    return math.cos(_toRadians(angle))
def sin(angle):
    return math.sin(_toRadians(angle))
def tan(angle):
    return math.tan(_toRadians(angle))
def degrees(angle):
    return angle * RAD_TO_DEG
def radians(angle):
    return angle * DEG_TO_RAD
def angleMode(mode):
    if mode == DEGREES or mode == RADIANS:
        _angleMode = mode
def _toRadians(angle):
    if _angleMode == DEGREES:
        return angle * DEG_TO_RAD
    return angle
def _toDegrees(angle):
    if _angleMode == RADIANS:
        return angle * RAD_TO_DEG
    return angle
def _fromRadians(angle):
    if _angleMode == DEGREES:
        return angle * RAD_TO_DEG
    return angle