import math

abs = abs
ceil = math.ceil


def constrain(n, low, high):
    return max(min(n, high), low)

def dist(**args):
    if len(args) == 4:
        # 2D
        return hypot(args[2] - args[0], args[3] - args[1])
    elif len(args) == 6:
        # 3D
        return hypot(args[3] - args[0], args[4] - args[1], args[5] - args[2])

exp = math.exp

floor = math.floor

def lerp(start, stop, amt):
    return amt * (stop - start) + start

log = math.log

def mag(x,y):
    return hypot(x,y)

def map(n, start1, stop1, start2, stop2, withinBounds=None):
    newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2
    if not withinBounds:
        return newval

    if start2 < stop2:
        return constrain(newval, start2, stop2)
    else:
        return constrain(newval, stop2, start2)
    
def norm(n,start,stop):
    return map(n,start,stop,0,1)

pow = math.pow

def sq(n):
    return n*n

sqrt = math.sqrt

def hypot(x, y, z = None):
    arguments = []
    if x:
        arguments.append(x)
    if y:
        arguments.append(y)
    if z:
        arguments.append(z)
    length = len(arguments)
    args = []
    maximum = 0
    for i in range(length):
        n = arguments[i]
        n = +n
        if n == math.inf or n == -math.inf:
            return math.inf
        n = abs(n)
        if n > maximum:
            maximum = n
        args.append(n)

    if maximum == 0:
        maximum = 1

    s = 0
    compensation = 0
    for j in range(length):
        m = args[j] / maximum
        summand = m * m - compensation
        preliminary = s + summand
        compensation = preliminary - s - summand
        s = preliminary
    
    return math.sqrt(s) * maximum

def fract(toConvert):
    sign = 0
    num = toConvert
    if not num or abs(num) == math.inf:
        return num
    elif num < 0:
        num = -num
        sign = 1
    
    if '.' in str(num) and 'e' not in str(num):
        toFract = str(num)
        toFract = float('0' + toFract[toFract.index('.'):])
        return abs(sign - toFract)
    elif num < 1:
        return abs(sign - num)
    else:
        return 0













