import math
from ..constants import RAD_TO_DEG,DEG_TO_RAD

class Vector:
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    @staticmethod
    def fromVector(v):
        return Vector(v.x, v.y, x.z)

    def __str__(self):
        return f'[{self.x}, {self.y}, {self.z}]'

    def copy(self):
        return Vector(self.x, self.y, self.z)

    def __add__(self, x):
        if type(x) is Vector:
            return Vector(self.x + x.x, self.y + x.y, self.z + x.z)
        else:
            return Vector(self.x + x, self.y + x, self.z + x)

    def __sub__(self, x):
        if type(x) is Vector:
            return Vector(self.x - x.x, self.y - x.y, self.z - x.z)
        else:
            return Vector(self.x - x, self.y - x, self.z - x)

    def __mul__(self, x):
        if type(x) is Vector:
            return Vector(self.x * x.x, self.y * x.y, self.z * x.z)
        else:
            return Vector(self.x * x, self.y * x, self.z * x)

    def __truediv__(self, x):
        if type(x) is Vector:
            return Vector(self.x / x.x, self.y / x.y, self.z / x.z)
        else:
            return Vector(self.x / x, self.y / x, self.z / x)

    def __floordiv__(self, x):
        if type(x) is Vector:
            return Vector(self.x // x.x, self.y // x.y, self.z // x.z)
        else:
            return Vector(self.x // x, self.y // x, self.z // x)

    def __inv__(self):
        return Vector(-self.x, -self.y, -self.z)

    def __mod__(self, x):
        if type(x) is Vector:
            return Vector(self.x % x.x, self.y % x.y, self.z % x.z)
        else:
            return Vector(self.x % x, self.y % x, self.z % x)

    def __pow__(self, x):
        if type(x) is Vector:
            return Vector(self.x**x.x, self.y**x.y, self.z**x.z)
        else:
            return Vector(self.x**x, self.y**x, self.z**x)

    def calculateRemainder2D(self, xComponent, yComponent):
        if xComponent:
            self.x = self.x % xComponent
        if yComponent:
            self.y = self.y % yComponent
        return self

    def calculateRemainder2D(self, xComponent, yComponent, zComponent):
        if xComponent:
            self.x = self.x % xComponent
        if yComponent:
            self.y = self.y % yComponent
        if zComponent:
            self.y = self.y % zComponent
        return self

    def mag(self):
        return math.sqrt(self.magSq())

    def magSq(self):
        return self.x * self.x + self.y * self.y + self.z * self.z

    def dot(self, x, y=0, z=0):
        if type(x) is Vector:
            return self.dot(x.x, x.y, x.z)
        return self.x * (x | 0) + self.y * (y | 0) + self.z * (z | 0)

    def cross(self, v):
        new_x = self.y * v.z - self.z * v.y
        new_y = self.z * v.x - self.x * v.z
        new_z = self.x * v.y - self.y * v.x
        return Vector(new_x, new_y, new_z)

    def dist(self, v):
        return (v.copy() - self).mag()

    def normalize(self):
        length = self.mag()
        if length != 0:
            self * (1 / length)
        return self

    def limit(self, maximum):
        mSq = self.magSq()
        if (mSq > maximum * maximum):
            self / math.sqrt(mSq) * maximum
        return self

    def setMag(self, n):
        self.normalize()
        self *= n
        return self

    def heading(self):
        h = math.atan2(self.y, self.x)
        return h

    def rotate(self, a):
        newHeading = self.heading() + a
        mag = self.mag()
        self.x = math.cos(newHeading) * mag
        self.y = math.sin(newHeading) * mag
        return self

    def angleBetween(self, v):
        dotmagmag = self.dot(v) / (self.mag() * v.mag())
        angle = math.acos(math.min(1, math.max(-1, dotmagmag)))
        angle = angle * math.sign(self.cross(v).z | 1)
        return angle

    def lerp(self, x, y, z, amt):
        if type(x) is Vector:
            return self.lerp(x.x, x.y, x.z, y)
        self.x += (x - self.x) * amt | 0
        self.y += (y - self.y) * amt | 0
        self.z += (z - self.z) * amt | 0
        return self

    def reflect(self, surfaceNormal):
        surfaceNormal.normalize()
        return self - (surfaceNormal * (2 * self.dot(surfaceNormal)))

    def array(self):
        return [self.x, self.y, self.z]

    def __eq__(self, v):
        return self.x == v.x and self.y == v.y and self.z == v.z

    @staticmethod
    def fromAngle(angle, length=1):
        return Vector(length * math.cos(angle), length * math.sin(angle))

    @staticmethod
    def fromAngles(theta, phi, length=1):
        cosPhi = math.cos(phi)
        sinPhi = math.sin(phi)
        cosTheta = math.cos(theta)
        sinTheta = math.sin(theta)

        return Vector(length * sinTheta * sinPhi, -length * cosTheta,
                      length * sinTheta * cosPhi)


def degrees(angle):
    return angle * RAD_TO_DEG


def radians(angle):
    return angle * DEG_TO_RAD


def dist(*args):
    if len(args) == 4:
            # 2D
        return hypot(args[2] - args[0], args[3] - args[1])
    elif len(args) == 4:
        # 3D
        return hypot(args[3] - args[0], args[4] - args[1], args[5] - args[2])





def collideLineLine(x1, y1, x2, y2, x3, y3, x4, y4):
    try:
        num = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3))
        den = ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1))
        uA =  num / den
        num = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) 
        den = ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1))
        uB =  num / den
        if uA >= 0 and uA <= 1 and uB >= 0 and uB <= 1:
            return True
        return False
    except :
        return False
